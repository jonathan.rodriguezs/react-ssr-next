import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import cards from './data/data.json'

//* initial state
const startState = {
  cards: []
}

//? Actions
export const initialCards = () => ({ type: 'INITIAL_CARDS', payload: cards })
export const addCard = card => ({ type: 'ADD_CARD', payload: card })

//? Reducers
export const reducer = (state = startState, { type, payload }) => {
  switch (type) {
    case 'INITIAL_CARDS':
      return {
        ...state,
        cards: payload
      }
    case 'ADD_CARD':
      return {
        ...state,
        cards: [...state.cards, payload]
      }
    default:
      return state
  }
}

//* create store
export const initStore = (initialState = startState) => {
  return createStore(
    reducer,
    initialState,
    composeWithDevTools(
      applyMiddleware(thunk)
    )
  )
}