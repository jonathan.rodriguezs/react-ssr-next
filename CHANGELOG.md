# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.2.0 (2019-08-17)


### Features

* **css:** add css support ([51aa5d2](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/51aa5d2))
* **pages:** declarate index page ([782bb96](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/782bb96))
* **redux:** connect app with redux ([e64f824](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/e64f824))
* **redux:** create actions ([2b7da13](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/2b7da13))
* **redux:** create reducer ([6b22b90](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/6b22b90))
* **redux:** create store ([0e35bda](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/0e35bda))
* **redux:** install dependencies ([05e0da1](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/05e0da1))
* add Card component ([ba04b6c](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/ba04b6c))
* **static:** create /static directory ([c01cda6](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/c01cda6))

## 1.1.0 (2019-08-17)


### Features

* **css:** add css support ([51aa5d2](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/51aa5d2))
* **pages:** declarate index page ([782bb96](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/782bb96))
* **redux:** connect app with redux ([e64f824](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/e64f824))
* **redux:** create actions ([2b7da13](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/2b7da13))
* **redux:** create reducer ([6b22b90](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/6b22b90))
* **redux:** create store ([0e35bda](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/0e35bda))
* **redux:** install dependencies ([05e0da1](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/05e0da1))
* add Card component ([ba04b6c](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/ba04b6c))
* **static:** create /static directory ([c01cda6](https://gitlab.com/jonathan.rodriguezs/react-ssr-next/commit/c01cda6))
