# React SSR practice

React SSR practice using the Next.js framework

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install the required dependencies and run the project.

```bash
npm install
npm run dev
```