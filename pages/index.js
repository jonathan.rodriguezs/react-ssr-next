import React, { Component } from 'react'
// Redux
import { bindActionCreators } from 'redux'
import { initialCards, addCard } from './../store'
// Components
import { connect } from 'react-redux'
import Card from '../components/Card/Card'
import Link from 'next/link'

import './index.css'

class Index extends Component {
  static async getInitialProps({ store }) {
    store.dispatch(initialCards())
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Link href='/static'>
            <img
              className="static-logo"
              src="/static/logo.png"
              alt="logo"
            />
          </Link>
        </header>
        <div className="Grid">
          {this.props.cards.map(card => {
            return (
              <Card key={card.id} />
            )
          })}
        </div>
      </div>
    )
  }
}


const mapStateToProps = ({ cards }) => ({ cards })
const mapDispatchToPops = dispatch => {
  return {
    initialCards: bindActionCreators(initialCards, dispatch),
    addCard: bindActionCreators(addCard, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToPops)(Index)